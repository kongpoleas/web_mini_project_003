import React from 'react';
import { Navbar, Container, Nav, Form, FormControl, Button, NavDropdown } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';

function NavMenu() {
	return (
		<Navbar bg='primary' variant="dark" >
			<Container>
				<Navbar.Brand as={NavLink} to='/'>
					AMS Redux
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse id='basic-navbar-nav'>
					<Nav className='me-auto'>
						<Nav.Link as={Link} to='/'>
							Home
						</Nav.Link>
						<Nav.Link as={NavLink} to='/article'>
							Article
						</Nav.Link>
						<Nav.Link as={NavLink} to='/author'>
							Author
						</Nav.Link>
						<Nav.Link as={NavLink} to='/welcome'>
							Category
						</Nav.Link>
						<NavDropdown title="Langauge" id="basic-nav-dropdown">
          				<NavDropdown.Item href="#action/3.1">Khmer</NavDropdown.Item>
          				<NavDropdown.Item href="#action/3.2">English</NavDropdown.Item>
          				{/* <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
          				<NavDropdown.Divider /> */}
          				{/* <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item> */}
        				</NavDropdown>
					</Nav>

					<Form className='d-flex'>
						<FormControl type='search' placeholder='Search' className='me-2' aria-label='Search' />
						<Button variant='outline-light'>Search</Button>
					</Form>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}

export default NavMenu;
