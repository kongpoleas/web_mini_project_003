import {Card, Button} from 'react-bootstrap'
function AuthorCard({article}){
    return(
    <Card className="m-3">
        <Card.Img variant="top" src={article.image} />
        <Card.Body>
        <Card.Title>{article.title}</Card.Title>
        <Card.Text>
            {article.description}
        </Card.Text>
        <Button variant="primary m-1">Read</Button>
        <Button variant="warning m-1">Edit</Button>
        <Button variant="danger m-1">Delete</Button>
        </Card.Body>
    </Card>
    );
}

export default AuthorCard;