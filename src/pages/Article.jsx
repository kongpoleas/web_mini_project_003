import React, { useEffect, useState } from 'react';
import { Container, Table, Button, Col, Row, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchAuthors, onInsertAuthor, onDeleteAuthor, onUpdateAuthor } from '../redux/actions/authorAction';
import { uploadImage } from '../services/authorService';
import { bindActionCreators } from 'redux';
import { onFetchArticle } from '../redux/actions/articleAction';

function Article() {
	const [authorName, setAuthorName] = useState('');
	const [email, setEmail] = useState('');
	const [imageURL, setImageURL] = useState('https://i1.wp.com/fnf-europe.org/wp-content/uploads/2020/01/panda-3857754_1920.jpg?ssl=1');
	const [imageFile, setImageFile] = useState(null);
	const [selectedId, setSelectedId] = useState('');

	const articles = useSelector((state) => state.articles.articles);
	const dispatch = useDispatch();
    const onFectch = bindActionCreators(onFetchArticle, dispatch);

    useEffect(() => {
        onFectch().then(console.log);
      }, []);

	let onAddOrUpdate = async (e) => {
		e.preventDefault();

		let newAuthor = {
			name: authorName,
			email,
		};

		if (imageFile) {
			let url = await uploadImage(imageFile);
			newAuthor.image = url;
		} else {
			newAuthor.image = imageURL;
		}

		if (selectedId) {
			dispatch(onUpdateAuthor(selectedId, newAuthor));
		} else {
			// insert new author
			dispatch(onInsertAuthor(newAuthor));
		}

		resetForm();
	};

	const onDelete = (id) => {
		// if selected author was deleted, we reset the form
		if (id === selectedId) resetForm();

		dispatch(onDeleteAuthor(id));
	};

	let resetForm = () => {
		setAuthorName('');
		setEmail('');
		setImageURL('https://i1.wp.com/fnf-europe.org/wp-content/uploads/2020/01/panda-3857754_1920.jpg?ssl=1');
		setImageFile(null);
		setSelectedId('');
	};

	return (
		<Container>
			<h1 className='my-3'>Article</h1>

			<Row>
				<Col md={8}>
					<Form>
						<Form.Group controlId='title'>
							<Form.Label>Title</Form.Label>
							<Form.Control
								type='text'
								placeholder='title'
								value={authorName}
								onChange={(e) => setAuthorName(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Form.Group controlId='email'>
							<Form.Label>Author</Form.Label>
							<Form.Control
								type='text'
								placeholder='author'
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

                        <Form.Group className="mb-3 my-2" controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Description</Form.Label>
                                <Form.Control as="textarea" rows={3} />
                        </Form.Group>

						<Button variant='primary' onClick={onAddOrUpdate} className="mt-2">
							{selectedId ? 'Update' : 'Post'}
						</Button>
					</Form>
				</Col>
				<Col md={4}>
					<img className='w-100' src={imageURL} />
					<Form>
						<Form.Group>
							<Form.File
								id='img'
								label='Choose Image'
								onChange={(e) => {
									let url = URL.createObjectURL(e.target.files[0]);
									setImageFile(e.target.files[0]);
									setImageURL(url);
								}}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>

			
		</Container>
	);
}

export default Article;
