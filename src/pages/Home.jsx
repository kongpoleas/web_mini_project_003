import { Button, Row, Col } from 'react-bootstrap';
import React, { useEffect }  from 'react';
import AuthorCard from '../components/AuthorCard';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { onFetchArticle } from '../redux/actions/articleAction';

function Home() {

const articles = useSelector((state) => state.articles.articles);
  const dispatch = useDispatch();
  const onFectch = bindActionCreators(onFetchArticle, dispatch);

  useEffect(() => {
    onFectch().then(console.log);
  }, []);

	return (
		<>
			<h3>Category</h3>
			<Button className="mx-2">metoo</Button>
			<Button>sports</Button>
			<Row>
				{articles.map((article)=>(
					<Col md={3}>
					<AuthorCard article={article} />
			   		</Col>
				))

				}
			</Row>
		</>

		);
	}

export default Home;
