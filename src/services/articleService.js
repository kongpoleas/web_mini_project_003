import api from "../utils/apis";

export const fetchArticle = async () => {
	let response = await api.get('/articles');
	return response.data.data;
};