import { fetchArticle } from "../../services/articleService";
import { articleActionType } from "./articleActionType";

export const onFetchArticle = () => async (dispatch) => {
	let articles = await fetchArticle();
	dispatch({
		type: articleActionType.FETCH_ARTICLE,
		payload: articles,
	});
};