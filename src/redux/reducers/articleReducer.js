import { articleActionType } from "../actions/articleActionType";

const initialState = {
	articles: [],
};

const articleReducer = (state = initialState, action) => {
	switch (action.type) {
		case articleActionType.FETCH_ARTICLE:
			return { ...state, articles: [...action.payload] };
		default:
			return state;
	}
};

export default articleReducer;