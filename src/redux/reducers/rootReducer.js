import { combineReducers } from 'redux';
import authorReducer from './authorReducer';
import articleReducer from './articleReducer';

const rootReducer = combineReducers({
    authors: authorReducer,
    articles: articleReducer
});

export default rootReducer;
